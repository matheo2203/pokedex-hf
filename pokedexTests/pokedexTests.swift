//
//  pokedexTests.swift
//  pokedexTests
//
//  Created by Mateo G on 17/12/23.
//

import XCTest
@testable import pokedex

final class pokedexTests: XCTestCase {
    
    var viewModel: MainViewViewModel!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        viewModel = MainViewViewModel()
    }
    
    override func tearDownWithError() throws {
        viewModel = nil
        try super.tearDownWithError()
    }
    
    func testLoadPokemonListIsSucess() async throws {
        // Given
        let limit = 30
        
        // When
        await viewModel.load()
        
        // Then
        XCTAssertEqual(viewModel.pokemonData.count, limit)
    }
    
    func testRestartPagination() async throws {
        // Given
        let limit = 30
        
        // When
        await viewModel.load(restart: true)
        
        // Then
        XCTAssertEqual(viewModel.lastIndex, limit)
    }
    
    func testGetPokemonAditionalInfoSuccess() async throws{
        // Given
        let name = "pikachu"
        let expectedResponse = Pokemon(id: 25, name: "pikachu", types: [PokemonType](), sprites: Sprite(front_default: ""), height: 4, weight: 60, base_experience: 112, stats: [PokemonStats]())
        
        // When
        let response = try? await viewModel.getAditionalInfo(name: name)
        
        // Then
        XCTAssertEqual(response?.name, expectedResponse.name)
        XCTAssertEqual(response?.id, expectedResponse.id)
        XCTAssertEqual(response?.height, expectedResponse.height)
        XCTAssertEqual(response?.weight, expectedResponse.weight)
        XCTAssertEqual(response?.base_experience, expectedResponse.base_experience)
    }
    
    func testGetPokemonAditionalInfoHasFailedIfDontSendCorrectName() async throws{
        // Given
        let name = ""
        
        // When
        let response = try? await viewModel.getAditionalInfo(name: name)
        
        // Then
        XCTAssertNil(response?.id)
    }
    
    func testSavePokemonDataInPreferencesIsSucess() {
        // Given
        var pokemonData = [Pokemon]()
        let pokemon = Pokemon(id: 25, name: "pikachu", types: [PokemonType](), sprites: Sprite(front_default: ""), height: 4, weight: 60, base_experience: 112, stats: [PokemonStats]())
        pokemonData.append(pokemon)
        
        // When
        try? viewModel.savePokemonDataInPreferences(pokemonData: pokemonData)
        
        // Then
        XCTAssertNoThrow(AppError.general)
    }
    
    func testFetchPokemonDataFromPreferences() async throws {
        // Given
        // When
        try? await viewModel.fetchPokemonDataFromPreferences()
        
        // Then
        XCTAssertNoThrow(AppError.general)
    }
}
