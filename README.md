<img src="SwiftUI-Pokedex/Assets.xcassets/AppIcon.appiconset/mac128.png">

# Pokedex with SwiftUI, MVVVM and combine
A Pokédex app created with the SwiftUI framework that utilizes data from [PokeAPI](https://pokeapi.co).

## Instalation guide 📱
- Clone the project by https via
- Run the command 'pod install'
- Open the project workspace
