//
//  pokedexApp.swift
//  pokedex
//
//  Created by Mateo G on 17/12/23.
//

import SwiftUI

@main
struct pokedexApp: App {
    var body: some Scene {
        WindowGroup {
            MainView(viewModel: MainViewViewModel())
        }
    }
}
