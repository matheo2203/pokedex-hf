//
//  PokemonListResponse.swift
//  pokedex
//
//  Created by Mateo G on 17/12/23.
//

import Foundation

struct PokemonListResponse: Decodable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [Results]?    
}

struct Results: Decodable {
    let name: String?
    let url: String?
}
