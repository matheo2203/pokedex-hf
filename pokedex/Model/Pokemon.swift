//
//  Pokemon.swift
//  pokedex
//
//  Created by Mateo G on 17/12/23.
//

import Foundation

struct Pokemon: Decodable, Identifiable, Encodable {
    static var totalFound = 0
    
    let id: Int?
    let name: String?
    let types: [PokemonType]?
    let sprites: Sprite?
    let height: Int?
    let weight: Int?
    let base_experience: Int?
    let stats: [PokemonStats]?
    
    init(id: Int?, name: String?, types: [PokemonType]?, sprites: Sprite?, height: Int?, weight: Int?, base_experience: Int?, stats: [PokemonStats]?) {
        self.id = id
        self.name = name
        self.types = types
        self.sprites = sprites
        self.height = height
        self.weight = weight
        self.base_experience = base_experience
        self.stats = stats
    }
}

struct Sprite: Decodable, Encodable {
    let front_default: String?
}

struct PokemonType: Decodable, Hashable, Encodable {
    let slot: Int?
    let type: Types?
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(slot)
        hasher.combine(type)
    }
    
    static func == (lhs: PokemonType, rhs: PokemonType) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

struct Types: Decodable, Hashable, Encodable {
    let name: String?
}

struct PokemonStats: Decodable, Hashable, Encodable {
    let base_stat: Int?
    let effort: Int?
    let stat: Stat?
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(base_stat)
        hasher.combine(effort)
        hasher.combine(stat)
    }
    
    static func == (lhs: PokemonStats, rhs: PokemonStats) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

struct Stat: Decodable, Hashable, Encodable {
    let name: String?
    let url: String
}

extension Pokemon: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Pokemon: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
    }
}
