//
//  Error.swift
//  pokedex
//
//  Created by Mateo G on 17/12/23.
//

import Foundation

enum AppError: Error {
    case serverError
    case noData
    case general
}
