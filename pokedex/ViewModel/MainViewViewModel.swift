//
//  MainViewViewModel.swift
//  pokedex
//
//  Created by Mateo G on 17/12/23.
//

import Foundation
import Combine

class MainViewViewModel: ObservableObject {
    @Published var pokemonData = [Pokemon]()
    @Published var lastIndex: Int = 0
    @Published var currentIndex: Int = 0
    
    private var urlSession = URLSession(configuration: .default)
    
    private let limit = 30
    private var offset = 0
    private var limitOffSet = 30
    
    private let preferencesKey = "pokemonData"
    
    private func restartPagination() {
        offset = 0
        Pokemon.totalFound = 0
    }
    
    func getPokemons() async throws -> PokemonListResponse {
        guard let url = URL(string: "https://pokeapi.co/api/v2/pokemon/?limit=\(limit)&offset=\(offset)")
        else { throw  AppError.general }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard (response as? HTTPURLResponse)?.statusCode == 200
        else { throw AppError.serverError }
        
        let jsonDecoder = JSONDecoder()
        guard let decoded = try? jsonDecoder.decode(PokemonListResponse.self, from: data)
        else { throw AppError.noData }
        
        self.offset += self.limit
        
        limitOffSet = decoded.count
        
        return decoded
    }
    
    func getAditionalInfo(name: String) async throws -> Pokemon{
        guard let url = URL(string: "https://pokeapi.co/api/v2/pokemon/\(name)")
        else{ throw AppError.general }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard (response as? HTTPURLResponse)?.statusCode == 200
        else { throw AppError.serverError }
        
        let jsonDecoder = JSONDecoder()
        guard let decoded = try? jsonDecoder.decode(Pokemon.self, from: data)
        else { throw AppError.noData }
        
        return decoded
    }
    
    func savePokemonDataInPreferences(pokemonData: [Pokemon]?) throws {
        do {
            let jsonEncoder = JSONEncoder()
            let encodedData = try jsonEncoder.encode(pokemonData)
            let userDefaults = UserDefaults.standard
            userDefaults.set(encodedData, forKey: preferencesKey)
        } catch {
            throw AppError.general
        }
    }
    
    @MainActor func fetchPokemonDataFromPreferences() throws {
        let userDefaults = UserDefaults.standard
        if let savedData = userDefaults.object(forKey: preferencesKey) as? Data {
            do{
                let jsonDecoder = JSONDecoder()
                pokemonData = try jsonDecoder.decode([Pokemon].self, from: savedData)
                
            } catch {
                throw AppError.general
            }
        }
    }
    
    @MainActor func load(restart: Bool = false) async throws {
        if restart {
            restartPagination()
            pokemonData.removeAll()
        }
        
        if offset < limitOffSet {
            do {
                let pokemonResponse = try await getPokemons()
                for pokemon in pokemonResponse.results! {
                    let pokemon = try await getAditionalInfo(name: pokemon.name!)
                    self.pokemonData.append(pokemon)
                }
                try savePokemonDataInPreferences(pokemonData: pokemonData)
                lastIndex = offset
                
            } catch {
                throw AppError.general
            }
        }
    }
}
