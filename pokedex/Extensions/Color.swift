//
//  Color.swift
//  pokedex
//
//  Created by Mateo G on 18/12/23.
//

import SwiftUI

extension Color {
    
    static func pokemon(type: Types?) -> Color {
        switch type?.name {
            case "fire": return Color(.red)
            case "poison": return Color(.green)
            case "water": return Color(.blue)
            case "electric": return Color(.yellow)
            case "psychic": return Color(.purple)
            case "normal": return Color(.orange)
            case "ground": return Color(.gray)
            case "flying": return Color(.teal)
            case "fairy": return Color(.pink)
        default: return  Color(.brown)
        }
    }
}
