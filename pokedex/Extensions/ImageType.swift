//
//  ImageType.swift
//  pokedex
//
//  Created by Mateo G on 19/12/23.
//

import SwiftUI

extension Image {
    
    static func pokemon(typeName: String?) -> Image {
        switch typeName{
            case "fire": return Image("ic_fire")
            case "dark": return Image("ic_dark")
            case "poison": return Image("ic_poison")
            case "water": return Image("ic_water")
            case "fighting": return Image("ic_fighting")
            case "ghost": return Image("ic_ghost")
            case "rock": return Image("ic_rock")
            case "electric": return Image("ic_electric")
            case "steel": return Image("ic_steel")
            case "psychic": return Image("ic_psychic")
            case "normal": return Image("ic_normal")
            case "ground": return Image("ic_ground")
            case "flying": return Image("ic_flying")
            case "fairy": return Image("ic_fairy")
            case "bug" : return Image("ic_bug")
            case "grass" : return Image("ic_grass")
            default: return Image("ic_normal")
        }
    }
}
