//
//  PokeCell.swift
//  pokedex
//
//  Created by Mateo G on 18/12/23.
//

import SwiftUI
import Kingfisher

struct PokeCell: View {
    let pokemon: Pokemon
    
    var body: some View {
        let color = Color.pokemon(type: pokemon.types?.first?.type)
        ZStack {
            VStack {
                HStack {
                    title
                    Spacer()
                    identifier
                }
                .padding(.top, 10)
                .padding(.horizontal, 10)
                HStack {
                    VStack {
                        ForEach(pokemon.types ?? [PokemonType](), id: \.self) { item in
                            AbilityCard(name: item.type?.name ?? "", color: color, type: item.type?.name ?? "normal")
                        }
                    }
                    if let url = URL(string: pokemon.sprites?.front_default ?? "") {
                        KFImage(url)
                            .placeholder {
                                ProgressView()
                            }
                            .resizable()
                            .scaledToFit()
                            .frame(width: 80, height: 110)
                    }
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(color)
            .cornerRadius(12)
            .shadow(color: color.opacity(0.7), radius: 6, x: 0.0, y: 0.0)
        }
    }
    
    var title: some View {
        Text(pokemon.name?.capitalized ?? "")
            .font(.caption).bold()
            .foregroundColor(.white)
    }
    
    var identifier: some View {
        Text("#\(String(pokemon.id ?? 0))" )
            .font(.caption).bold()
            .foregroundColor(.white)
    }
}

struct AbilityCard: View {
    let name: String
    let color: Color
    let type: String
    
    var body: some View {
        HStack {
            Image.pokemon(typeName: type)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 15, height: 15)
                .foregroundColor(.white)
            Text(name)
                .font(.caption)
                .foregroundColor(.white)
                .padding(.trailing, 2)
        }
        .padding(.leading, 10)
        .foregroundColor(color)
        .overlay(
            RoundedRectangle(cornerRadius: 25)
                .fill(Color.white.opacity(0.25))
        )
    }
}
