//
//  SearchBar.swift
//  pokedex
//
//  Created by Mateo G on 19/12/23.
//

import SwiftUI

struct SearchBar: View {
    @Binding var searchText: String

    var body: some View {
        HStack {
            TextField("Search", text: $searchText)
                .padding(8)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .padding([.leading, .trailing], 8)
        }
        .padding()
    }
}
