//
//  DetailView.swift
//  pokedex
//
//  Created by Mateo G on 18/12/23.
//

import SwiftUI
import Kingfisher

struct DetailView: View {
    let pokemon: Pokemon
    
    var body: some View {
        ScrollView {
            VStack {
                HStack {
                    if let url = URL(string: pokemon.sprites?.front_default ?? "") {
                        KFImage(url)
                            .placeholder {
                                ProgressView()
                            }
                            .scaledToFit()
                    }
                    Image.pokemon(typeName: pokemon.types?.first?.type?.name ?? "")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 30, height: 30)
                        .foregroundColor(.white)
                    Spacer()
                    Text(pokemon.name ?? "")
                        .font(.title)
                        .foregroundColor(Color.pokemon(type: pokemon.types?.first?.type))
                        .fontWidth(.condensed)
                        .padding()
                    
                }
                VStack {
                    StatCell(title: "Experience", value: "\(pokemon.base_experience ?? 0) XP")
                    StatCell(title: "Weight", value: "\(pokemon.weight ?? 0) kg")
                    StatCell(title: "Height", value: "\(pokemon.height ?? 0) ft")
                    VStack {
                        ForEach(pokemon.stats ?? [PokemonStats](), id: \.self) { item in
                            StatCell(title: item.stat?.name ?? "", value: "\(item.base_stat ?? 0)")
                        }
                    }
                    Spacer()
                }
                .navigationBarTitle("Pokemon Detail", displayMode: .inline)
            }
        }
    }
}

struct StatCell: View {
    var title: String
    var value: String
    
    var body: some View {
        HStack {
            Text(title.capitalized)
                .font(.headline)
                .fontWidth(.condensed)
                .foregroundColor(.gray)
            
            Spacer()
            
            Text(value)
                .font(.body)
                .fontWeight(.bold)
                .fontWidth(.condensed)
                .foregroundColor(.black)
        }
        .padding([.top, .bottom], 8)
        .padding([.leading, .trailing], 16)
    }
}
