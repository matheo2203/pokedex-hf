//
//  MainView.swift
//  pokedex
//
//  Created by Mateo G on 17/12/23.
//

import SwiftUI

struct MainView: View {
    @ObservedObject var viewModel = MainViewViewModel()
    @State private var searchText = ""
    @State private var networkMonitor = NetworkMonitor()
    
    private let gridItems = [GridItem(.flexible()), GridItem(.flexible())]
    
    private var filteredPokemon: [Pokemon] {
        if searchText.isEmpty {
            return viewModel.pokemonData
        }
        return viewModel.pokemonData.filter { $0.name!.localizedCaseInsensitiveContains(searchText)}
    }
    
    var body: some View {
        NavigationStack {
            VStack {
                Text("Pokedex")
                    .font(.largeTitle)
                    .foregroundColor(.black)
                    .padding()
                    .fontWidth(.condensed)
                Text("Use the advanced search to find Pokemon by type, weakness, ability and more!")
                    .font(.headline)
                    .foregroundColor(.gray)
                    .padding()
                    .fontWidth(.condensed)
            }
            SearchBar(searchText: $searchText)
            ScrollViewReader { scrollViewProxy in
                ScrollView {
                    LazyVGrid(columns: gridItems, spacing: 20) {
                        ForEach(filteredPokemon) { pokemon in
                            NavigationLink(destination: DetailView(pokemon: pokemon)) {
                                PokeCell(pokemon: pokemon)
                            }
                        }
                    }
                    .onChange(of: viewModel.lastIndex) { newIndex in
                        viewModel.currentIndex = newIndex
                        if viewModel.currentIndex == viewModel.lastIndex && networkMonitor.isConnected {
                            refreshData()
                        }
                    }
                }
            }
        }
        .padding(.horizontal, 8)
        .ignoresSafeArea(.all, edges: .bottom)
        .task {
            if networkMonitor.isConnected {
                await viewModel.load(restart: true)
            } else {
                try? viewModel.fetchPokemonDataFromPreferences()
            }
        }
    }
    
    private func refreshData() {
        Task {
            await viewModel.load()
        }
    }
}
